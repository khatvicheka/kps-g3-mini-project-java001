package utils;

import java.io.IOException;
import java.util.Scanner;

public  class Validation {
    // Method for input only numbers
    public static  int validationNumber(String st){
        Scanner sc = new Scanner(System.in);
        int num=0;
        while (true){
            System.out.print(st);
            String str = sc.nextLine();
            if (str.matches("[0-9][0-9]*")){
                num = Integer.parseInt(str);
                break;
            }
            else
                System.out.println("\nPLEASE INPUT NUMBERS\n");
        }
        return num;
    }
    // End Method for input only numbers

    // Method for input Double and integer
    public static   double validationDouble(String st){
        Scanner sc = new Scanner(System.in);
        double num=0;
        while (true){
            System.out.print(st);
            String str = sc.nextLine();
            if (str.matches("\\d+(\\.\\d+)?")){
                num = Double.parseDouble(str);
                break;
            }else if(str.matches("[0-9][0-9]*")){
                num = Integer.parseInt(str);
                break;
            }
            else
                System.out.println("\nPLEASE INPUT NUMBERS\n");
        }
        return num;
    }
    // End Method for input Double and integer

    // Method for input only String
    public static  String validationString(String st){
        Scanner sc = new Scanner(System.in);
        String values;
        while (true){
            System.out.print(st);
            String str = sc.nextLine();
            //^(.*\s+.*)+$
            if (str.matches("[a-zA-Z]+\\.?")){
                values =str;
                break;
            }
            else
                System.out.println("\nPLEASE INPUT STRING WITH NOT WHITESPACE\n");
        }
        return values;
    }


    //End Method for input only String

    // method for press enter to continue...
    public  void promptEnterKey(){
        System.out.println("Press \"ENTER\" to continue...");
        try {
            int read = System.in.read(new byte[2]);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    // End method for press enter to continue...

}
