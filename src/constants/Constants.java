package constants;

public class Constants {
    public static final String txtClassName = "\t\t\t\t\t\t\t __  ___ .______     _______.     _______  ____   \n" +
            "\t\t\t\t\t\t\t|  |/  / |   _  \\   /       |    /  _____||___ \\  \n" +
            "\t\t\t\t\t\t\t|  '  /  |  |_)  | |   (----`   |  |  __    __) | \n" +
            "\t\t\t\t\t\t\t|    <   |   ___/   \\   \\       |  | |_ |  |__ <  \n" +
            "\t\t\t\t\t\t\t|  .  \\  |  |   .----)   |      |  |__| |  ___) | \n" +
            "\t\t\t\t\t\t\t|__|\\__\\ | _|   |_______/        \\______| |____/  \n" +
            "                                                  ";
    public static final String projectName = "\t\t\t\t\t\t\t\t\t\t\uD835\uDD4A\uD835\uDD65\uD835\uDD60\uD835\uDD54\uD835\uDD5C \uD835\uDD44\uD835\uDD52\uD835\uDD5F\uD835\uDD52\uD835\uDD58\uD835\uDD56\uD835\uDD5E\uD835\uDD56\uD835\uDD5F\uD835\uDD65 \uD835\uDD4A\uD835\uDD6A\uD835\uDD64\uD835\uDD65\uD835\uDD56\uD835\uDD5E";
    public static final String Welcome = "\t\t\t\t\t\t\t\t\t\t\t\t\uD835\uDD4E\uD835\uDD56\uD835\uDD5D\uD835\uDD54\uD835\uDD60\uD835\uDD5E\uD835\uDD56 \uD835\uDD65\uD835\uDD60";
}
