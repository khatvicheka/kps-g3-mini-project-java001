package Views;

import models.Stock;
import org.apache.log4j.*;
import org.nocrala.tools.texttablefmt.*;
import utils.List;
import utils.Validation;
import utils.dbConnection;
import constants.Constants;
import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Process {
    static Connection c = null;
    static PreparedStatement preparedStatement = null;
    Scanner scanner = new Scanner(System.in);


    //Welcome Method
    public void Welcome(){
        System.out.println(Constants.Welcome);
        System.out.println(Constants.projectName);
        System.out.println(Constants.txtClassName);
    }

    //Loading method
    public void Loading(String message, long delayTime){
        for (int i=0;i<message.length();i++){
            System.out.print(message.charAt(i));
            try {
                Thread.sleep(delayTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println();
        System.out.println("Current time loading : 3");
    }

    //Display menu text
    public void displayMenu(){
        CellStyle cs = new CellStyle(CellStyle.HorizontalAlign.left,
                CellStyle.AbbreviationStyle.crop,CellStyle.NullStyle.emptyString);
        Table t = new Table(1, BorderStyle.UNICODE_DOUBLE_BOX,
                ShownBorders.ALL,false,"");
        Logger.getRootLogger().setLevel(Level.OFF);

        String st = "*)Display W)rite R)ead U)pdate D)elete F)irst P)revious N)ext L)ast" +
                " S)earch G)oto Se)t row Sa)ave B)ackup R)estore H)elp E)xit";

        t.addCell(st,cs);
        System.out.println(t.render());
    }
    //Choose option
    public void Menu(){

        c = dbConnection.connectDB();
        String option;
        readDataIntoList();
        int currentPage=0;
        int totalPage=0;
        int gotoAnyPage =0;
        int pageSize=2;
        int newPageSize;
        PaginatedList<Stock> paginatedList = new PaginatedList<Stock>(List.stocks,pageSize);
        while (true){
            displayMenu();

            System.out.print("Command --> ");
            option = scanner.next();

            switch (option.toLowerCase()){
                case "*" :
                        //Display data
                        java.util.List<Stock> page = paginatedList.firstPage();
                        currentPage=paginatedList.getCurrentPage();
                        totalPage=paginatedList.numberOfPages();
                        displayData(page,currentPage,totalPage);
                        break;
                case "w" :
                        WriteData();
                        paginatedList = new PaginatedList<Stock>(List.stocks,pageSize);
                    break;
                case "_1m":
                        write1M();
                        paginatedList = new PaginatedList<Stock>(List.stocks,pageSize);
                    break;
                case "r" : readData();
                    break;
                case "u" :
                        Updated();
                        paginatedList = new PaginatedList<Stock>(List.stocks,pageSize);
                    break;
                case "d" :
                        Deleted();
                        paginatedList = new PaginatedList<Stock>(List.stocks,pageSize);

                    break;
                case "f" :
                        page = paginatedList.firstPage();
                        currentPage=paginatedList.getCurrentPage();
                        totalPage=paginatedList.numberOfPages();
                        displayData(page,currentPage,totalPage);
                    break;
                case "p" :
                        page = paginatedList.previousPage();
                        currentPage=paginatedList.getCurrentPage();
                        totalPage=paginatedList.numberOfPages();
                        displayData(page,currentPage,totalPage);
                    break;
                case "n" :
                        page = paginatedList.nextPage();
                        currentPage=paginatedList.getCurrentPage();
                        totalPage=paginatedList.numberOfPages();
                        displayData(page,currentPage,totalPage);
                    break;
                case "l" :
                        page = paginatedList.lastPage();
                        currentPage=paginatedList.getCurrentPage();
                        totalPage=paginatedList.numberOfPages();
                        displayData(page,currentPage,totalPage);
                    break;
                case "s" :
                        searchData();
                    break;
                case "g" :
                        gotoAnyPage= Validation.validationNumber("Enter Number of row :");
                        page = paginatedList.gotoAnyPage(gotoAnyPage);
                        currentPage=paginatedList.getCurrentPage();
                        totalPage=paginatedList.numberOfPages();
                        displayData(page,currentPage,totalPage);
                    break;
                case "se" :
                        newPageSize = Validation.validationNumber("Enter New Row :");
                        pageSize=newPageSize-1;
                        paginatedList = new PaginatedList<Stock>(List.stocks,pageSize);
                    break;
                case "sa" :
                    break;
                case "b" : break;
                case "re" : break;
                case "h" : menuHelp();
                    break;
                case "e" :
                    System.exit(0);
                    break;
            }
        }
    }

    //Help menu method
    public void menuHelp(){
        CellStyle cs = new CellStyle(CellStyle.HorizontalAlign.left, CellStyle.AbbreviationStyle.crop,
                CellStyle.NullStyle.emptyString);
        Table t = new Table(1, BorderStyle.CLASSIC, ShownBorders.ALL, false, "");
        Logger.getRootLogger().setLevel(Level.OFF);

        String menus[] = {"1. Press * : Display all record of products",
                "2. Press w: Add new product",
                "Press w#proName-unitPrice-qty : Shortcut for add new product",
                "3. Press r: Read content any content",
                "Press r#proId : Shortcut for read product by Id",
                "4. Press u: Update Data",
                "5. Press d: Delete Data",
                "Press d#proId : Shortcut for delete product by Id",
                "6. Press f: Display first page",
                "7. Press p:  Display previous page",
                "8. Press n:  Display next page",
                "9. Press l:  Display last page",
                "10. Press s: Search product",
                "11. Press sa: Save record to file",
                "12. Press ba: Backup data",
                "13. Press re: Restore data",
                "14. Press h: Help"};

        for(int i=0; i<menus.length; i++){
            t.addCell(menus[i],cs);
        }
        System.out.println(t.render());
    }
    //write data 10000 records
    public void write1M(){
        int id =List.stocks.size()+1;
//        System.out.println();
        for (int i=1;i<=10000;i++){
            List.stocks.add(new Stock(i+id,"Coca",10,20,"2020-12-22"));
        }
    }

    //write data
    public void WriteData(){
        int id = Validation.validationNumber("Enter Id: ");
        String name = Validation.validationString("Enter name :");
        int price = Validation.validationNumber("Enter Price :");
        int qty = Validation.validationNumber("Enter qty :");
        System.out.print("Enter Import Date :");
        scanner.nextLine();
        String date = scanner.nextLine();
        List.stocks.add(new Stock(id,name,price,qty,date));
    }
    //Shortcut write data
//    public void shortcutWrite(){
//
//    }
    //Save data
//    public
    //Read data from database into ArrayList
    public void readDataIntoList(){
        try {
            preparedStatement = c.prepareStatement("SELECT * FROM book");
            ResultSet rs = preparedStatement.executeQuery(); //use executeQuery() because we are just fetching the data

            while (rs.next()) { // read data row by row, and next() method return true if the resultSet still has next row of data
                int bookId = rs.getInt("book_id");
                String bookName = rs.getString("book_name");
                int unitPrice = rs.getInt("unit_price");
                int quantity = rs.getInt("stock_quantity");
                String importedDate = rs.getString("imported_date");
                List.stocks.add(new Stock(bookId,bookName,unitPrice,quantity,importedDate));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    //display data
    public void displayData(java.util.List<Stock> page ,int currentPage, int totalPage){
        CellStyle cs = new CellStyle(CellStyle.HorizontalAlign.left,
                CellStyle.AbbreviationStyle.crop,CellStyle.NullStyle.emptyString);
        Table t = new Table(5, BorderStyle.UNICODE_DOUBLE_BOX,
                ShownBorders.ALL,false,"");
        Logger.getRootLogger().setLevel(Level.OFF);

        t.addCell("ID",cs);
        t.addCell("NAME",cs);
        t.addCell("UNIT PRICE",cs);
        t.addCell("QUANTITY",cs);
        t.addCell("IMPORTED DATE",cs);

        for (Stock sc : page){
            t.addCell(String.valueOf(sc.getId()),cs);
            t.addCell(sc.getBookName(),cs);
            t.addCell(String.valueOf(sc.getUnitPrice()),cs);
            t.addCell(String.valueOf(sc.getStockQuantity()),cs);
            t.addCell(sc.getImportedDate(),cs);
        }
        System.out.println(t.render());
        String infoPage = "Page "+currentPage+
                " of "+totalPage+
                "\t\t\t\t\tTotal record:"+totalPage;
        CardInfo(infoPage,45);
    }

    //TODO READ
    public void readData(){
        int find =0;
        String y;
        CellStyle cs = new CellStyle(CellStyle.HorizontalAlign.left,
                CellStyle.AbbreviationStyle.crop,CellStyle.NullStyle.emptyString);
        Table t = new Table(1, BorderStyle.UNICODE_DOUBLE_BOX,
                ShownBorders.SURROUND,false,"");
        Logger.getRootLogger().setLevel(Level.OFF);

        int Rid = Validation.validationNumber("Read by ID: ");
        for (int i=0;i<List.stocks.size();i++){
            if(Rid == List.stocks.get(i).getId()){
                find =1;
                t.addCell("ID             : "+List.stocks.get(i).getId(),cs);
                t.addCell("Name           : "+List.stocks.get(i).getBookName(),cs);
                t.addCell("Unit Price     : "+List.stocks.get(i).getUnitPrice(),cs);
                t.addCell("Qty            : "+List.stocks.get(i).getStockQuantity(),cs);
                t.addCell("Imported Date  : "+List.stocks.get(i).getImportedDate(),cs);
            }
        }System.out.println(t.render());
        if(find ==0){
            System.out.println("ID not found.");
        }
    }
    //TODO SEARCH
    public void searchData(){
        int find =0;
        CellStyle cs = new CellStyle(CellStyle.HorizontalAlign.left,
                CellStyle.AbbreviationStyle.crop,CellStyle.NullStyle.emptyString);
        Table t = new Table(5, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,
                ShownBorders.ALL,false,"");
        Logger.getRootLogger().setLevel(Level.OFF);

        String SName = Validation.validationString("Search by Name: ");

            t.addCell("ID",cs);
            t.addCell("NAME",cs);
            t.addCell("UNIT PRICE",cs);
            t.addCell("QUANTITY",cs);
            t.addCell("IMPORTED DATE",cs);

        for (int i=0;i<List.stocks.size();i++){
            if(List.stocks.get(i).getBookName().contains(SName)){
                find = 1;
                t.addCell(String.valueOf(List.stocks.get(i).getId()),cs);
                t.addCell(List.stocks.get(i).getBookName(),cs);
                t.addCell(String.valueOf(List.stocks.get(i).getUnitPrice()),cs);
                t.addCell(String.valueOf(List.stocks.get(i).getStockQuantity()),cs);
                t.addCell(List.stocks.get(i).getImportedDate(),cs);
            }

        }
        if(find ==0){
            System.out.println("Name not found");
        }else {
            System.out.println(t.render());
        }
    }
    
    /// By Lon dara.
    /// Method for Update ArrayList.
    public void Updated(){
        int index;
        int ch;
        int ID;
        ID = Validation.validationNumber("Product's ID : ");
        System.out.println("What do you want to Update ?");
        MenuForUpdate();/// calling Menu For Update.
        ch = Validation.validationNumber("Option (1-6) : ");
        switch (ch){
            case 1:/// Update all value in ArrayList.
                index = GetIndex(ID);
                if(index!=-1) {// control when wrong ID input.
                    Card(index);
                    String bookName;int unitPrice;int stockQuantity;String importedDate;
                    bookName =Validation.validationString("=> Enter BookName : ");
                    unitPrice = Validation.validationNumber("=> Enter UnitPrice : ");
                    stockQuantity = Validation.validationNumber("=> Enter StockQuantity : ");
                    scanner.nextLine();
                    System.out.print("=> ImportedDate : ");
                    importedDate = scanner.nextLine();
//                    System.out.print("Are you sure to update this record ? [Yes] Enter 'y' or [No] = Enter 'n':");
                    String makeSure = Validation.validationString("Are you sure to update this record ? [Yes] Enter 'y' or [No] = Enter 'n':");
                    switch (makeSure.toLowerCase()) {
                        case "y":
                            List.stocks.set(index, new Stock(ID, bookName, unitPrice, stockQuantity, importedDate));// set to ArrayList stocks.
                            Card(index);// calling card show information for value in ArrayList.
                            CardInfo("Updated Successfully!",25);/// calling Card for show message when update.
                            break;
                        case "n":
                            break;
                    }
                }else{
                    System.out.println("=> ID : "+ID+" not found.");
                }
                break;
            case 2:/// Update Name in ArrayList.
                index = GetIndex(ID);
                String makeSure;
                if(index!=-1) {// control when wrong ID input.
                    Card(index);
                    String newName = Validation.validationString("=> Enter Product's Name: ");
                    makeSure = Validation.validationString("Are you sure to update this record ? [Yes] Enter 'y' or [No] = Enter 'n':");
                    switch (makeSure.toLowerCase()) {
                        case "y":
                            List.stocks.set(index, new Stock(ID, newName, (int) List.stocks.get(index).getUnitPrice(), List.stocks.get(index).getStockQuantity(), List.stocks.get(index).getImportedDate()));
                            Card(index);// calling card show information for value in ArrayList.
                            CardInfo("Updated Successfully!",25);/// calling Card for show message when update.
                            //NewUpdate.add(ID); open
                            break;
                        case "n":
                            break;
                    }
                }else{
                    System.out.println("=> ID : "+ID+" not found.");
                }
                break;
            case 3:/// Update UnitPrice in ArrayList.
                index = GetIndex(ID);
                if(index!=-1) {// control when wrong ID input.
                    Card(index);
                    int newPrice = Validation.validationNumber("=> Enter Product's UnitPrice : ");
                    makeSure = Validation.validationString("Are you sure to update this record ? [Yes] Enter 'y' or [No] = Enter 'n':");
                    switch (makeSure.toLowerCase()) {
                        case "y":
                            List.stocks.set(index, new Stock(ID, List.stocks.get(index).getBookName(), newPrice, List.stocks.get(index).getStockQuantity(), List.stocks.get(index).getImportedDate()));// set to ArrayList stocks.
                            Card(index);// calling card show information for value in ArrayList.
                            CardInfo("Updated Successfully!",25);/// calling Card for show message when update.
                            //NewUpdate.add(ID);
                            break;
                        case "n":
                            break;
                    }
                }else{
                    System.out.println("=> ID : "+ID+" not found.");
                }
                break;
            case 4:/// update StockQuantity in ArrayList.
                index = GetIndex(ID);
                if(index!=-1) {// control when wrong ID input.
                    int newQty = Validation.validationNumber("=> Enter StockQuantity : ");
                    makeSure = Validation.validationString("Are you sure to update this record ? [Yes] Enter 'y' or [No] = Enter 'n':");
                    switch (makeSure.toLowerCase()) {
                        case "y":
                            List.stocks.set(index, new Stock(ID, List.stocks.get(index).getBookName(), (int) List.stocks.get(index).getUnitPrice(), newQty, List.stocks.get(index).getImportedDate()));// set to ArrayList stocks.
                            Card(index);// calling card show information for value in ArrayList.
                            CardInfo("Updated Successfully!",25);/// calling Card for show message when update.
                            //NewUpdate.add(ID);
                            break;
                        case "n":
                            break;
                    }
                }else{
                    System.out.println("=> ID : "+ID+" not found.");
                }
                break;
            case 5:
                break;
            default:
                System.out.println("Choose Option  Again!!!!");
                break;
        }
    }

    /// Method for Delete data in arrayList.
    public void Deleted(){
        int ID = Validation.validationNumber("Enter Product's ID : ");
        int index = GetIndex(ID);
        Card(index);
        String ch = Validation.validationString("Are you sure to update this record ? [Yes] Enter 'y' or [No] = Enter 'n':");
        switch (ch.toUpperCase()){
            case "N":
                break;
            case "Y":
                List.stocks.remove(index);
                CardInfo("Deleted Successfully!",25);
                //NewDelete.add(ID);
                break;
            default:
                System.out.println("Choose Again!!!");
                break;
        }
    }

    /// get Index in arrayList send to Update and Delete function or methods.
    /// I Update and Delete by index of ArrayList.
    public int GetIndex(int ID){
        int index = 0;
        for(int i=0;i<List.stocks.size();i++){
            if(List.stocks.get(i).id==ID){
                index = i;
                break;
            }else{
                index = -1;
            }
        }
        return index;
    }

    /// Card for show Message when Update.
    public void CardInfo(String info,int minWidth){
        CellStyle Style = new CellStyle(CellStyle.HorizontalAlign.center);
        Table tt = new Table(1, BorderStyle.DESIGN_CURTAIN,
                ShownBorders.SURROUND);
        tt.setColumnWidth(0, minWidth, 50);
        tt.addCell(info);
        System.out.println(tt.render());
    }

    /// Menu for Update.
    public void MenuForUpdate(){
        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.center);

        Table t = new Table(1, BorderStyle.UNICODE_DOUBLE_BOX, ShownBorders.SURROUND);
        t.setColumnWidth(0, 70, 70);
        t.addCell("        1). All   2). Name  3). UnitPrice  4). StockQuantity   5). Back to Menu");
        System.out.println(t.render());
    }

    /// Card for show Information
    public void Card(int index) {
        CellStyle numberStyle = new CellStyle(CellStyle.HorizontalAlign.right);
        Table t = new Table(1, BorderStyle.UNICODE_DOUBLE_BOX, ShownBorders.SURROUND);
        t.setColumnWidth(0, 30, 30);
        t.addCell("Book_id :  "+String.valueOf(List.stocks.get(index).getId()));
        t.addCell("Book_name : "+List.stocks.get(index).getBookName());
        t.addCell("Unit_price : "+String.valueOf(List.stocks.get(index).getUnitPrice()));
        t.addCell("Stock_quantity :  "+String.valueOf(List.stocks.get(index).getStockQuantity()));
        t.addCell("Imported_date :  "+List.stocks.get(index).getImportedDate());
        System.out.println(t.render());
    }
//// end By Lon dara.

}

