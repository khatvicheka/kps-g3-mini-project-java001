package Views;
import utils.Validation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class PaginatedList<T> {
    private List<T> list;
    private List<List<T>> listOfPages;
    private int currentPage = 0;

    public PaginatedList(List<T> list, int pageSize) {
        this.list = list;
        initPages(pageSize);
    }

    // setup page size
    public void initPages(int pageSize) {
        if (list == null || listOfPages != null) {
            return;
        }

        if (pageSize <= 0 || pageSize > list.size()) {
            pageSize = list.size();
        }

        int numOfPages = (int) Math.ceil((double) list.size() / (double) pageSize);
        listOfPages = new ArrayList<List<T>>(numOfPages);
        for (int pageNum = 0; pageNum < numOfPages;) {
            int from = pageNum * pageSize;
            int to = Math.min(++pageNum * pageSize, list.size());
            listOfPages.add(list.subList(from, to));
        }
    }


    //get data from list
    public List<T> getPage(int pageNumber) {
        if (listOfPages == null) {
            return Collections.emptyList();
        }
        else if(pageNumber < 0){
            currentPage= listOfPages.size()-1;
            List<T> page = listOfPages.get(currentPage);
            return page;
        }
        else if (pageNumber >=listOfPages.size()){
            currentPage = 0;
            List<T> page = listOfPages.get(currentPage);
            return page;
        }
        else {
            currentPage = pageNumber;//1
            List<T> page = listOfPages.get(currentPage);
            return page;
        }
    }
    // Show number of page
    public int numberOfPages() {
        if (listOfPages == null) {
            return 0;
        }

        return listOfPages.size();
    }
    //next Page
    public List<T> nextPage() {
        List<T> page = getPage(++currentPage);
        return page;
    }
    //previousPage
    public List<T> previousPage() {
        List<T> page = getPage(--currentPage);
        return page;
    }
    //First Page or current page
    public List<T> firstPage(){
        currentPage = 0;
        List<T> page = listOfPages.get(currentPage);
        return page;
    }
    //Last page
    public List<T> lastPage(){
        currentPage= listOfPages.size()-1;
        List<T> page = listOfPages.get(currentPage);
        return page;
    }
    //goto any page
    public List<T> gotoAnyPage(int numberPage){
        currentPage = --numberPage;
        List<T> page = listOfPages.get(--numberPage);
        return page;
    }

    public int getCurrentPage() {
        return currentPage+1;
    }
}

